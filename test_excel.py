#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
import csv

"""
Test extraction of excel files from Omid
"""

def get_xls(src_dir):
    for root, dirs, files in os.walk(src_dir): #walk through all folders and subfolders starting at src_dir
        for f in files:
            if f.split('.')[-1] == 'xlsx': #check if file is excel file
                try:
                    df = pd.read_excel(os.path.join(root, f)) 
                    print(df)
                except BaseException as e:
                    print("File {} could not be read".format(os.path.join(root, f)))

src_dir = "../"
get_xls(src_dir)
